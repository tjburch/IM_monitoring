################################################
#             IM Spy Buffer Check              #
#          tyler.james.burch@cern.ch           #
################################################

# Run as:
# python IMbufferCheck.py -f InputFile.dat [-o Output.pdf] [-r] [-v]
# InputFile.dat may be an IM spy buffer or a Test Vector
import sys
import argparse
import time
import logging
parser = argparse.ArgumentParser('Check for consistencies within a file')
parser.add_argument('-f', dest='inFile', required=True,
                    help='Input File')
parser.add_argument('-o', dest='outputName', required=False,
                    help='Custom Output Name', default="_DEFAULTNAME_")
parser.add_argument("-r", dest="enableROOT",
                    default=False, action="store_true",
                    help="Write ROOT file of histograms - if disabled, no output files are written")
parser.add_argument("-v", "--verbose",  dest="verbose_logging",
                    default=False, action="store_true",
                    help="Increase verbosity to debug")
args = parser.parse_args()



# Error by default, debug if option passed
if args.verbose_logging: logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.ERROR)
    print "Checking %s for IM problems..." % args.inFile

class callcounted(object):
    """Decorator to determine number of calls for a method
    Used here for logging information
    """

    def __init__(self,method):
        self.method=method
        self.counter=0

    def __call__(self,*args,**kwargs):
        self.counter+=1
        return self.method(*args,**kwargs)

logging.error = callcounted(logging.error)


# Try to import ROOT, if not then crash
if args.enableROOT:
    try:
        from ROOT import *
    except ImportError:
        print "[-r] flag passed, but ROOT is unavailable."
        print "Failed importing ROOT - exiting now."
        sys.exit(1)


f = open(args.inFile, 'r')

# Create Histograms for inSpy and outSpy
if args.enableROOT:
    histos = {}
    for channel in range(16):
        name = "inSpy_nWords_ch{0}".format(channel)
        histos[name]  = TH1F(name, "", 501, 0, 500)
    for channel in range(16):
        name = "outSpy_nWords_ch{0}".format(channel)
        histos[name]  = TH1F(name, "", 501, 0, 500)

############### Define some functions to use ####################################
def id_increase_check(last_l1, this_l1):
    """
    :param last_l1: string, previous l1id ( -1 for first event)
    :param this_l1: string, this l1id
    :param has_looped: bool, false if this error type hasn't happened yet (loop over)
    :return: True if l1id does not increase by 1
    """
    if last_l1 == -1:
        # First Event for this channel
        return False
    elif last_l1 == 0:
        # last L1ID not valid
        return False
    elif int(last_l1, 16)+1 != int(this_l1, 16):
        return True
    else: return False


def format_line(line):
    """
    Formats line to something usable
    :param line: str, current line
    :return: line stripped
    """
    return line.split("-")[1].split()[0][2:]


def check_dead_channel(i, content):
    """
    Catches if a channel dies in the middle of reading
    :param i: line number
    :param content: all lines
    :return: True if this line and the next 9 are all 00000000
    """
    dead_check = 0
    for check_line in range(10):
        if "HHHHHHHHHH IM" in content[i+check_line]: return False
        elif "00000000" in content[i+check_line]: dead_check+=1
    if dead_check == 9: return True
    else: return False


def loop_errors(output_message, has_looped, loop_this_event, line_number, channel, extra_info=""):
    """Gives debug if looping hasn't happened, error if it has
    :param output_message: string - first line message
    :param line_number: line number
    :return: True for has_looped
    """
    if not has_looped:
        logging.debug("{0}\n"
                      "    Line number: {1}\n"
                      "    Buffer likely loops over here - not an issue\n"
                      "    {2}"
                      .format(output_message, line_number, extra_info))
    elif has_looped and loop_this_event:
        logging.debug("{0}\n"
                      "    Line number: {1}\n"
                      "    Buffer looped this event - not an issue\n"
                      "    {2}"
                      .format(output_message, line_number, extra_info))
    else:
        logging.error("{0}\n"
                      "    Line number: {1}\n "
                      "    {2}"
                      "    {3}"
                      .format(output_message, line_number, channel, extra_info))
        logging.debug("Buffer had already looped over, so this is probably an ERROR! ")

    return True


############### Main loops for input types ####################################
def inner_detector_check(content):
    """
    Iterates over the lines and checks inspys
    :param content: readlines output
    """
    logging.debug("HHHHHHHH InSpys Start Here HHHHHHHH")

    header, data, event, channel_active, first_eof= False, False, False, False, True
    has_looped, loop_this_event = False, False
    head_l1, nwords, last_l1 = 0, 0, -1
    n_good_events, n_bad_events, channel_fails= 0, 0, 0
    channel = 0
    logging.debug("------> Channel 0")

    i = 0
    while i < len(content):
        line = content[i]
        if "[FTK_IM" in line and "inspy" in line:
            this_channel = line.split("inspy")[1].split()[0][2:][:-1]

            if channel != int(this_channel):
                # Conditions if we move to a new channel
                if not channel_active: logging.debug("Channel %s Inactive" % channel)
                if channel_active: logging.debug("Number of potential problem events in channel %s: %s"
                                                 % (channel,channel_fails))
                # increment total bad events
                n_bad_events += channel_fails
                # Reinitalize some variables for next channel
                event, channel_active, first_eof, has_looped = False, False, True, False
                channel_fails = 0
                channel += 1
                last_l1 = -1
                logging.debug("------> Channel {0}".format(channel))

            line = line.split("-")[1].split()[0][2:] # Skim extra contents out of line
            FAIL = False

            # Check if a channel is active based on *any* nonzero events
            if channel_active is not True and not "00000000" in line: channel_active = True

            # Check if a channel dies
            if channel_active is True and not "00000000" in line and check_dead_channel(i, content):
                header,data,event, loop_this_event = False,False,False,False
                logging.error("Data stopped in middle of channel %s!\n"
                              "    This probably means FIFO's never filled\n"
                              % channel)

            # Note if first e0f word has happened or not
            if not event and line.startswith("e0f00000") and first_eof: first_eof = False

            # If first e0f has happened, but we're in an event, error
            elif not event and line.startswith("e0f00000") and not first_eof:
                has_looped = loop_errors("Second e0f word before b0f",
                                         has_looped, loop_this_event, i, channel)
                loop_this_event = has_looped
                last_l1 = 0
                channel_fails += 1

            # Start of Event
            if line.startswith("b0f00000"):
                # Check that a channel break won't happen on any of the header words
                for checkLine in range(7):
                    if "HHHHHHHHHH IM" in content[i+checkLine]:
                        i= i+checkLine
                        continue

                # Turn off data since in header, reset n_data_words
                n_data_words,data = 0,False
                # If b0f while in an event, throw an error
                if event:
                    has_looped = loop_errors("b0f came in middle of event!",
                                             has_looped, loop_this_event, i, channel,
                                             "L1ID: %s" % head_l1)
                    last_l1 = 0
                    channel_fails+=1

                # start an event and get some other info from header
                header, event, loop_this_event = True, True, False
                header_length = int(format_line(content[i+2]),16)
                header_word_seen = 0
                head_l1 =  format_line(content[i+6]) # Get header l1id based on position wrt b0f



            # Header starts here
            if header:
                # Count words until getting to data
                header_word_seen += 1
                if header_word_seen is header_length+2:
                    header,data = False,True

            if event:

                # Body starts here
                if data:
                    # Add nword for every word in data
                    nwords+=1

                # end of event
                if line.startswith("e0f00000"):
                    # Fais if l1id doesn't increase right, doesn't fail if previous was bad
                    if id_increase_check(last_l1, head_l1):
                        has_looped = loop_errors("L1ID did not increase by 1 ",
                                                 has_looped, loop_this_event, i, channel)
                        loop_this_event = has_looped

                    # Get some info from the trailer
                    n_data_words = int(format_line(content[i-2]), 16)

                    if not loop_this_event:
                        # Fill histogram
                        if args.enableROOT:
                            histos["inSpy_nWords_ch{0}".format(channel)].Fill(n_data_words)

                        n_good_events= n_good_events+1
                        last_l1 = head_l1
                    else:
                        # add fail count, set last_l1 to not cause error
                        channel_fails += 1
                        last_l1=0

            # e0f execution
            if line.startswith("e0f00000"): # Reset at e0f (even if not in event)
                # Reset defaults for next event
                header, data, event = False, False, False
                head_l1, nwords = 0,0
        i+=1
    # Output useful information
    logging.debug("\n-> inSpy Total Events: %i\n"
                  "-> Number of Good Events (all channels): %i\n"
                  "-> Number of Potential Problem Events (all channels): %i"
                  % (n_good_events+n_bad_events,n_good_events,n_bad_events))


def im_output_check(content):
    """ Iterates over the lines, checks outspys
    :param content: readlines output
    """
    logging.debug("HHHHHHHH OutSpys Start Here HHHHHHHH")

    header, data, trailer,event,channel_active, first_eof = False, False, False, False, False, True
    has_looped, loop_this_event = False, False
    head_l1, trail_l1, nwords, last_l1 = 0,0,0,-1
    n_good_events,n_bad_events, channel_fails = 0,0,0
    channel =0
    logging.debug("------> Channel 0")

    i=0
    while i < len(content):
        line = content[i]
        if "[FTK_IM" in line and "outspy" in line:
            thisChannel = line.split("outspy")[1].split()[0][2:][:-1]

            # Conditions if we move to a new channel
            if channel != int(thisChannel):
                # Conditions if we move to a new channel
                if not channel_active: logging.debug("Channel %s Inactive" % channel)
                if channel_active: logging.debug("Number of potential problem events in channel %s: %s"
                                                 % (channel, channel_fails))
                # increment total bad events
                n_bad_events += channel_fails
                # Reinitalize variables for next channel
                event,channel_active, first_eof, has_looped = False, False, True, False
                channel_fails = 0
                channel +=1
                last_l1 = -1
                logging.debug("------> Channel {0}".format(channel))

            line = line.split("-")[1].split()[0][2:] # Skim extra contents out of line
            FAIL = False

            # Check if a channel is active based on *any* nonzero events
            if channel_active is not True and not "00000000" in line: channel_active = True

            # Check if a channel dies
            if channel_active is True and not "00000000" in line and check_dead_channel(i, content):
                    header, data, event, loop_this_event = False, False, False, False
                    logging.error("Data stopped in middle of channel %s!\n"
                              "    This probably means FIFO's never filled\n"
                              % channel)

            # Note if first e0f word has happened or not
            if not event and line.startswith("e0f00000") and first_eof: first_eof = False

            # If first e0f has happened, but we're in an event, error
            elif not event and line.startswith("e0f00000") and not first_eof:

                has_looped = loop_errors("Second e0f word before b0f",
                                         has_looped, loop_this_event, i, channel)
                loop_this_event = has_looped
                last_l1 = 0
                channel_fails += 1

            # Start of Event
            if line.startswith("b0f00000"):
                # Check that a channel break won't happen in the header
                for checkLine in range(6):
                    if "HHHHHHHHHH IM" in content[i+checkLine]:
                        i= i+checkLine
                        continue

                # Turn off data since we're in header, reset data words
                nwords, data, trailer = 0, False, False

                # If b0f while in an event, throw an error
                if event:
                    has_looped = loop_errors("b0f came in middle of event!",
                                             has_looped, loop_this_event, i, channel,
                                             "L1ID: %s" % head_l1)
                    last_l1 = 0
                    channel_fails+=1
                    n_bad_events+=1

                # If not, start an event and get some other info from header
                header, event, loop_this_event = True, True, False
                header_word_seen = 0
                head_l1 =  format_line(content[i+3]) # Get header l1id based on position wrt b0f

            if header:
                data = False
                header_word_seen +=1
                # after 8 words, switch to data
                if header_word_seen is 8:
                    header,data = False,True

            if event:

                # Body starts here
                if not line.startswith("e0da") and data:
                    # Add nWord for every word in data
                    nwords+=1

                # Trailer starts here
                if line.startswith("e0da"):
                    data, trailer = False, True
                    if "HHHHHHHHHH" in content[i+1] :
                        event, data = False, False
                        continue
                    else: trail_l1 =  format_line(content[i+1]) # get trailer l1id

                # End of event conditions
                if line.startswith("e0f00000"):

                    # Check both l1id's exist
                    if not head_l1 and trail_l1: has_looped = logging.error("Trailer l1id but no header - ID:", trail_l1)
                    if  head_l1 and not trail_l1: has_looped = logging.error("Header l1id but no trailer - ID: ", head_l1)
                    # If they do, do some checks
                    if head_l1 and trail_l1:
                        if head_l1 == trail_l1: # Good event case
                            # Fails if l1id doesn't increase right, doesn't fail if previous was bad
                            FAIL = id_increase_check(last_l1, head_l1)
                            last_l1 = head_l1
                        if head_l1 != trail_l1:
                            has_looped = loop_errors("Header L1ID does not match trailer.\n    header l1:  %s\n    trailer l1: %s"
                                                     % (head_l1, trail_l1),
                                                     has_looped, loop_this_event, i, channel)
                            FAIL = True
                            last_l1 = 0
                        if not FAIL:
                            # Fill histogram
                            if args.enableROOT: histos["outSpy_nWords_ch{0}".format(channel)].Fill(nwords) # Fills histogram
                            n_good_events= n_good_events+1
                            last_l1 = head_l1
                        else:
                            n_bad_events+=1
                            channel_fails += 1
                            last_l1 = 0


            if line.startswith("e0f00000"): # Reset at e0f (even if not in event)
                header, data, trailer, event = False, False, False, False
                head_l1, trail_l1, nwords = 0,0,0
        i+=1
    # output useful information
    logging.debug("\n-> inSpy Total Events: %i\n"
                  "-> Number of Good Events (all channels): %i\n"
                  "-> Number of Potential Problem Events (all channels): %i"
                  % (n_good_events+n_bad_events,n_good_events,n_bad_events))


def test_vector_check(content):
    """ Checks for consistency in Test Vector
    Needs updating
    """
    if args.enableROOT: nWords = TH1F("nWords","",501,0,500)
    header, data, trailer,event = False, False, False,False
    head_l1, trail_l1, nDataWords, lastL1 = 0,0,0,-1
    nGoodEvents,nBadEvents = 0,0

    for i,line in enumerate(content):
        FAIL = False

        if line.startswith("b0f00000"):
            if event == True: # Throw error statements if already in an event (no fail, start new event here)
                print "b0f came in middle of event"
                print "     Line number: ", i,"\n    l1ID: ", head_l1
            header,event = True,True  # We want to start an event here
            headerWordSeen = 0
            head_l1 =  content[i+3]
        if header:
            # counts words until end of header
            headerWordSeen +=1
            if headerWordSeen is 8:
                header,data = False,True
        if event:
            if not line.startswith("e0da") and data:
                nDataWords+=1

            if line.startswith("e0da"):
                data, trailer = False, True
                if "HHHHHHHHHH" in content[i+1] : continue
                else: trail_l1 =  formatLine(content[i+1]) # get trailer l1id


            if line.startswith("e0f00000"):
                if not head_l1 and trail_l1: print "Trailer but no header"
                if  head_l1 and not trail_l1: print "Header, no trailer"
                if head_l1 and trail_l1:
                    if int(head_l1,16) == int(trail_l1,16): # Good event case
                        FAIL = idIncCheck(lastL1,head_l1)   #fails if l1id doesn't increase right, not fail if previous was bad
                        if args.enableROOT: nWords.Fill(nDataWords)
                        lastL1 = head_l1
                    else: # header doesn't match trailer
                        print "Header L1ID does not match trailer.\n    header l1:  ", head_l1,"    trailer l1: ", trail_l1
                        FAIL = True
                        lastL1 = 0


        if line.startswith("e0f00000"):
            if not FAIL: nGoodEvents= nGoodEvents+1
            else: nBadEvents = nBadEvents+1
            # Reset defaults for next event
            header, data, trailer, event = False, False, False, False
            head_l1, trail_l1, nDataWords = 0,0,0

    print "-> Total events: ", nGoodEvents+nBadEvents
    print "-> Number of Good Events: ", nGoodEvents
    print "-> Number of Problem Events: ", nBadEvents

    nWords.GetXaxis().SetTitle("nWords")
    if args.enableROOT:
        outfile = TFile(outputname+'.root','recreate')
        outfile.cd()
        nWords.Write()
    c1= TCanvas()
    nWords.Draw()
    #c1.Print(outputName+".pdf")

def hist_setup(hist):
    """ Sets up histograms to be drawn
    :param hist: TH1F
    :return:
    """
    hist.GetXaxis().SetRange(
                    TMath.FloorNint(histos["inSpy_nWords_ch"+str(channel)].FindFirstBinAbove(-1)),
                    TMath.CeilNint(histos["inSpy_nWords_ch"+str(channel)].FindLastBinAbove(0))
                )
    hist.GetXaxis().SetTitle("nWords")


def write_histos():
    """ Writes histograms to PDF. If root enabled, also writes root file
    """
    if args.enableROOT:
        if args.outputName == "_DEFAULTNAME_":
            timestr = time.strftime("%Y%m%d%H%M")
            outputName = args.inFile.split('.')[0]+'_'+timestr+'_nWords'
        else:
            outputName = args.outputName

        gStyle.SetOptStat(111111)
        c1 = TCanvas()
        pdfStarted = False
        outfile = TFile(outputName+'.root','recreate')

        # InSpys
        for channel in range(15):
            if histos["inSpy_nWords_ch"+str(channel)].GetEntries() > 0:
                hist_setup(histos["inSpy_nWords_ch"+str(channel)])
                histos["inSpy_nWords_ch"+str(channel)].Draw()

                # If PDF not created yet, make one
                if histos["inSpy_nWords_ch"+str(channel)].GetEntries() > 0 and not pdfStarted:
                    c1.Print(outputName+".pdf(","pdf")
                    pdfStarted = True
                else: c1.Print(outputName+".pdf")

                # If root enabled, write histograms to root file
                histos["inSpy_nWords_ch"+str(channel)].Write()

        # Figure out the last populated outspy channel
        for channel in range(15):
            if histos["outSpy_nWords_ch"+str(channel)].GetEntries() > 0:
                lastChannel = channel

        # Outspys
        for channel in range(15):
            if histos["outSpy_nWords_ch"+str(channel)].GetEntries() > 0:
                hist_setup(histos["outSpy_nWords_ch"+str(channel)])
                histos["outSpy_nWords_ch"+str(channel)].Draw()

                if channel == lastChannel: c1.Print(outputName+".pdf)","pdf")
                else: c1.Print(outputName+".pdf")
                histos["outSpy_nWords_ch"+str(channel)].Write()


if __name__ in '__main__':

    content = f.readlines()

    spyBuffer = False

    for line in content:
        line = line.replace(" ","") #clear any whitespace
        if len(line) > 9:  spyBuffer = True # TVs have only 8 characters, else spybuffer

    if spyBuffer:
        inner_detector_check(content)
        im_output_check(content)
        write_histos()
        if not logging.error.counter: print "No errors found in IM spybuffers"
    else:
        test_vector_check(content)
