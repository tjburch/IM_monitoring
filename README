This script is designed to check consistency in IM spybuffers or Test Vectors.

At lab 4 you can run as:
python IMbufferCheck.py -f InputFile.dat [-o OutputName] [-r]

If no output name given, will publish as ./FILENAME_DATE_nWords.pdf
If -r flag passed, publishes histograms to a root file as well (same name)
Don't put any extensions on output file name

At p1, ROOT functionality does not exist, so do not pass [-r]

Currently, the script does the following things:

For inSpys:
- Indicates if a channel is inactive
- Checks every b0f00000 is followed by an e0f000000
- Checks e0f00000 is not repeated - on events where buffer loops over, this is fine
- Gets number of data words from trailer, fills histogram for each channel
- Ensures that L1ID's increases monotonically
- Outputs number of "bad" events per channel (b0f before e0f, L1ID increase problem)
- Outputs number of "good" events, and total events

For outSpys:
- Indicates if a channel is inactive
- Makes sure every b0f00000 is followed by an e0f000000
- Checks e0f00000 is not repeated - on events where buffer loops over, this is fine
- Gets number of data words from *counting*, fills histogram for each channel
- Ensures that L1ID's increases monotonically
- Outputs number of "bad" events per channel (b0f before e0f, L1ID increase problem)
- Outputs number of "good" events, and total events

For Test Vectors:
- Makes sure every b0f00000 is followed by an e0f000000
- Gets number of data words from *counting*, fills histogram
- Ensures that L1ID's increases monotonically
- Outputs number of "bad" (b0f before e0f, L1ID increase problem)
- Outputs number of "good" events, and total events

Writes histograms of nWords for each channel to indicated .pdf (and root file if option passed)

If you need histograms for p1 spybuffer, move the it to an area with ROOT and run.